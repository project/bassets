; Drupal.org release file.
core = 7.x
api = 2

; Contrib Modules

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc5"

projects[conditional_fields][subdir] = "contrib"
projects[conditional_fields][download][type] = git
; One commit before https://drupal.org/node/1957884 (Conditional Fields with multiple forms) was reverted again.
projects[conditional_fields][download][revision] = 2e8fa2552aa1fb6333c2fadd9acd1d077e10babd
projects[conditional_fields][download][branch] = 7.x-3.x

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.12"

projects[date][subdir] = "contrib"
projects[date][version] = "2.10"

projects[diff][subdir] = "contrib"
projects[diff][version] = "3.3"

projects[distro_update][subdir] = "contrib"
projects[distro_update][version] = "1.0-beta4"

projects[entity][subdir] = "contrib"
projects[entity][version] = 1.8

projects[entity_rules][subdir] = "contrib"
projects[entity_rules][version] = "1.0-alpha1"

projects[exif][subdir] = "contrib"
projects[exif][version] = "1.8"
; Readonly mode in edit-mode.
projects[exif][patch][2174155] = https://www.drupal.org/files/issues/exif-2174155-2.patch

projects[features][subdir] = "contrib"
projects[features][version] = "2.10"
projects[features][patch][] = https://www.drupal.org/files/features-1411678-export-permissions-with-role-14.patch

projects[field_extrawidgets][subdir] = "contrib"
projects[field_extrawidgets][version] = "1.1"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.5"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.4"

projects[filehash][subdir] = "contrib"
projects[filehash][version] = "1.6"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.3"

projects[link][subdir] = "contrib"
projects[link][version] = "1.4"

projects[media][subdir] = "contrib"
projects[media][version] = "2.10"

projects[multiform][subdir] = "contrib"
projects[multiform][version] = "1.4"
; Form processing ignores validation errors
projects[multiform][patch][] = https://www.drupal.org/files/multiform-1663212_1.patch

projects[oauth][subdir] = "contrib"
projects[oauth][version] = "3.4"

projects[plupload][subdir] = "contrib"
projects[plupload][version] = "1.7"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.10"

projects[rules_conditional][subdir] = "contrib"
projects[rules_conditional][version] = "1.0-beta2"

projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.22"

projects[search_api_db][subdir] = "contrib"
projects[search_api_db][version] = "1.6"

projects[search_api_solr][subdir] = "contrib"
projects[search_api_solr][version] = "1.12"

projects[services][subdir] = "contrib"
projects[services][version] = "3.20"

projects[services_client][subdir] = "contrib"
projects[services_client][version] = "1.0-beta1"
projects[services_client][patch][] = https://www.drupal.org/files/services_client.1976516.4.patch

projects[services_entity][subdir] = "contrib"
projects[services_entity][version] = "2.0-alpha8"

projects[services_search_api][subdir] = "contrib"
projects[services_search_api][version] = "1.0-alpha1"
projects[services_search_api][patch][] = https://www.drupal.org/files/services_search_api-1991936.patch
; Issue https://www.drupal.org/node/2010646
projects[services_search_api][patch][] = https://www.drupal.org/files/services_search_api-1.patch
projects[services_search_api][patch][] = https://www.drupal.org/files/issues/services_search_api-2072691-6.patch
projects[services_search_api][patch][] = https://www.drupal.org/files/services_search_api-2074119-1.patch

projects[setup][subdir] = "contrib"
projects[setup][version] = "1.x-dev"
projects[setup][patch][] = https://www.drupal.org/files/setup-2016897-1.patch
projects[setup][patch][] = https://www.drupal.org/files/setup_2020701_1.patch

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.2"

projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.0"

projects[uuid_features][subdir] = "contrib"
projects[uuid_features][version] = "1.0-rc1"

projects[views][subdir] = "contrib"
projects[views][version] = "3.18"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.4"


; Bassets modules

projects[bassets_server][subdir] = "contrib"
projects[bassets_server][version] = "1.1"

projects[bassets_file_usage][subdir] = "contrib"
projects[bassets_file_usage][version] = "1.0"

projects[bassets_scc][subdir] = "contrib"
projects[bassets_scc][version] = "1.0"

projects[bassets_setup][subdir] = "contrib"
projects[bassets_setup][version] = "1.0"
