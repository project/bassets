#!/bin/bash
set -o errexit
#set -x

__DIR__="$(cd "$(dirname "${0}")"; echo $(pwd))"

rm -rf -v modules libraries
drush make build-dev.make --no-core --concurrency=2 --no-patch-txt  --working-copy --contrib-destination=. "$@"

reset_modules() {
  cd ${__DIR__}/modules/contrib/bassets_server && git reset --hard
  cd ${__DIR__}/modules/contrib/bassets_file_usage && git reset --hard
  cd ${__DIR__}/modules/contrib/bassets_scc && git reset --hard
  cd ${__DIR__}/modules/contrib/bassets_setup && git reset --hard
}

echo "Git reset --hard bassets modules?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) reset_modules; break;;
        No ) exit;;
    esac
done

