includes[drupal-org] = drupal-org.make

projects[bassets_server][subdir] = "contrib"
projects[bassets_server][download][type] = git
projects[bassets_server][download][url] = tobiasb@git.drupal.org:project/bassets_server.git
projects[bassets_server][type] = "module"
projects[bassets_server][download][branch] = 7.x-1.x

projects[bassets_file_usage][subdir] = "contrib"
projects[bassets_file_usage][download][type] = git
projects[bassets_file_usage][download][url] = tobiasb@git.drupal.org:project/bassets_file_usage.git
projects[bassets_file_usage][type] = "module"
projects[bassets_file_usage][download][branch] = 7.x-1.x

projects[bassets_scc][subdir] = "contrib"
projects[bassets_scc][download][type] = git
projects[bassets_scc][download][url] = tobiasb@git.drupal.org:project/bassets_scc.git
projects[bassets_scc][type] = "module"
projects[bassets_scc][download][branch] = 7.x-1.x

projects[bassets_setup][subdir] = "contrib"
projects[bassets_setup][download][type] = git
projects[bassets_setup][download][url] = tobiasb@git.drupal.org:project/bassets_setup.git
projects[bassets_setup][type] = "module"
projects[bassets_setup][download][branch] = 7.x-1.x
